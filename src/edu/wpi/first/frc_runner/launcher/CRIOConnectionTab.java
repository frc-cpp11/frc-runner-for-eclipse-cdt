package edu.wpi.first.frc_runner.launcher;

import java.io.File;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class CRIOConnectionTab extends AbstractLaunchConfigurationTab {
	public static final String TEAM_NUMBER_NAME = "Team Number";
	public static final String TEAM_NUMBER_ID = "edu.wpi.first.frc_runner.teamNumber";

	public static final String USER_NAME = "cRIO Username";
	public static final String USER_ID = "edu.wpi.first.frc_runner.username";

	public static final String PASS_NAME = "cRIO Password";
	public static final String PASS_ID = "edu.wpi.first.frc_runner.password";

	public static final String FILE_NAME = "File to Deploy";
	public static final String FILE_ID = "edu.wpi.first.frc_runner.file";

	private Text teamID;
	private Text user;
	private Text pass;
	private Text file;
	private Label crioIP;

	@Override
	public String getName() {
		return "cRIO Connection";
	}

	@Override
	public String getMessage() {
		return "Configure the cRIO Launch";
	}

	public static String getIPByTeam(int team) {
		return "10." + (int) (team / 100) + "." + (team % 100) + ".2";
	}

	private void addTeamID(Composite parent) {
		Composite composite = createDefaultComposite(parent, 3);
		Label ownerLabel = new Label(composite, SWT.NONE);
		ownerLabel.setText(TEAM_NUMBER_NAME);
		teamID = new Text(composite, SWT.SINGLE | SWT.BORDER);
		crioIP = new Label(composite, SWT.NONE);
		GridData gd = new GridData();
		gd.widthHint = 50;
		teamID.setLayoutData(gd);
		teamID.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				getTeamID();
			}
		});
	}

	private void addFileChooser(Composite parent) {
		Composite composite = createDefaultComposite(parent, 3);
		Label ownerLabel = new Label(composite, SWT.NONE);
		ownerLabel.setText(FILE_NAME);
		file = new Text(composite, SWT.SINGLE | SWT.BORDER);
		Button choose = new Button(composite, SWT.PUSH);
		choose.setText("Browse");
		GridData gd = new GridData();
		gd.widthHint = 50;
		choose.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				File f = getFile(new File(file.getText()));
				if (f != null) {
					file.setText(f.getPath());
				}
			}
		});
		file.setLayoutData(gd);
	}

	private File getFile(File startingDirectory) {
		FileDialog dialog = new FileDialog(getShell(), SWT.OPEN | SWT.SHEET);
		if (startingDirectory != null) {
			dialog.setFileName(startingDirectory.getPath());
		}
		String file = dialog.open();
		if (file != null) {
			file = file.trim();
			if (file.length() > 0) {
				return new File(file);
			}
		}
		return null;
	}

	private Text addText(Composite parent, String name) {
		Composite composite = createDefaultComposite(parent, 2);
		Label ownerLabel = new Label(composite, SWT.NONE);
		ownerLabel.setText(name);
		Text t = new Text(composite, SWT.SINGLE | SWT.BORDER);
		GridData gd = new GridData();
		gd.widthHint = 150;
		t.setLayoutData(gd);
		return t;
	}

	private int getTeamID() {
		try {
			int team = Integer.parseInt(teamID.getText());
			if (team >= 25600 || team <= 0) {
				setErrorMessage("'" + teamID.getText()
						+ "' is an invalid team number!");
				crioIP.setText("");
			} else {
				// Probably a team
				setErrorMessage(null);
				crioIP.setText("(" + getIPByTeam(team) + ")");
				return team;
			}
		} catch (NumberFormatException ex) {
			setErrorMessage("'" + teamID.getText() + "' is not a valid number!");
			crioIP.setText("");
		}
		return -1;
	}

	private Composite createDefaultComposite(Composite parent, int cols) {
		Composite composite = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		layout.numColumns = cols;
		composite.setLayout(layout);

		GridData data = new GridData();
		data.verticalAlignment = GridData.FILL;
		data.horizontalAlignment = GridData.FILL;
		composite.setLayoutData(data);

		return composite;
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		setControl(composite);
		GridLayout layout = new GridLayout();
		composite.setLayout(layout);
		GridData data = new GridData(GridData.FILL);
		data.grabExcessHorizontalSpace = true;
		composite.setLayoutData(data);

		addTeamID(composite);
		user = addText(composite, USER_NAME);
		pass = addText(composite, PASS_NAME);
		addFileChooser(composite);
	}

	@Override
	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {
		initializeFrom(configuration);
		performApply(configuration);
	}

	@Override
	public void initializeFrom(ILaunchConfiguration configuration) {
		try {
			int team = configuration.getAttribute(TEAM_NUMBER_ID, 0);
			if (team > 0) {
				teamID.setText(team + "");
			} else {
				teamID.setText("");
			}
			getTeamID();
		} catch (CoreException e) {
		}
		try {
			user.setText(configuration.getAttribute(USER_ID, "root"));
			pass.setText(configuration.getAttribute(PASS_ID, ""));
			file.setText(configuration.getAttribute(FILE_ID, ""));
		} catch (CoreException e) {
		}
	}

	@Override
	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		configuration.setAttribute(TEAM_NUMBER_ID, getTeamID());
		configuration.setAttribute(USER_ID, user.getText());
		configuration.setAttribute(PASS_ID, pass.getText());
		configuration.setAttribute(FILE_ID, file.getText());
	}

	@Override
	public boolean canSave() {
		return getTeamID() > 0 && file.getText().trim().length() > 0;
	}

	@Override
	public boolean isValid(ILaunchConfiguration configuration) {
		try {
			int team = configuration.getAttribute(TEAM_NUMBER_ID, 0);
			if (team >= 25600 || team <= 0) {
				throw new Exception();
			}
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
}
