package edu.wpi.first.frc_runner.launcher;

import java.io.IOException;

import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.SafeRunner;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IStreamListener;
import org.eclipse.debug.core.model.IStreamMonitor;
import org.eclipse.debug.core.model.IStreamsProxy;

import edu.wpi.first.frc_runner.Activator;

public class CRIOStreamsProxy implements IStreamsProxy {
	private NetConsole console;
	private NetConsoleOutputMonitor monitor;

	public CRIOStreamsProxy(String ip) {
		try {
			console = new NetConsole(ip);
			monitor = new NetConsoleOutputMonitor();
			monitor.startMonitoring();
		} catch (Exception e) {
			console = null;
			monitor = null;
		}
	}

	void kill() {
		if (monitor != null) {
			monitor.kill();
			monitor.close();
			monitor = null;
		}
	}

	@Override
	public IStreamMonitor getErrorStreamMonitor() {
		return null;
	}

	@Override
	public IStreamMonitor getOutputStreamMonitor() {
		return monitor;
	}

	@Override
	public void write(String s) throws IOException {
		if (console != null) {
			console.send(s);
		}
	}

	private class NetConsoleOutputMonitor implements IStreamMonitor {
		private ListenerList fListeners = new ListenerList();
		private Thread fThread;
		private boolean fKilled = false;

		public synchronized void addListener(IStreamListener listener) {
			fListeners.add(listener);
		}

		protected void close() {
			if (fThread != null) {
				Thread thread = fThread;
				fThread = null;
				try {
					thread.join();
				} catch (InterruptedException ie) {
				}
				fListeners = new ListenerList();
			}
		}

		private void read() {
			while (!fKilled) {
				try {
					String read = console.read();
					if (read != null && read.length() > 0) {
						getNotifier().notifyAppend(read);
					}
				} catch (IOException ioe) {
					if (!fKilled) {
						DebugPlugin.log(ioe);
					}
					return;
				} catch (NullPointerException e) {
					if (!fKilled && fThread != null) {
						Activator.log(e);
					}
					return;
				}
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
				}
			}
		}

		protected void kill() {
			fKilled = true;
		}

		public synchronized void removeListener(IStreamListener listener) {
			fListeners.remove(listener);
		}

		protected void startMonitoring() {
			if (fThread == null) {
				fThread = new Thread(new Runnable() {
					public void run() {
						read();
					}
				});
				fThread.setDaemon(true);
				fThread.setPriority(Thread.MIN_PRIORITY);
				fThread.start();
			}
		}

		private ContentNotifier getNotifier() {
			return new ContentNotifier();
		}

		class ContentNotifier implements ISafeRunnable {
			private IStreamListener fListener;
			private String fText;

			public void handleException(Throwable exception) {
				Activator.log(exception);
			}

			public void run() throws Exception {
				fListener.streamAppended(fText, NetConsoleOutputMonitor.this);
			}

			public void notifyAppend(String text) {
				if (text == null)
					return;
				fText = text;
				Object[] copiedListeners = fListeners.getListeners();
				for (int i = 0; i < copiedListeners.length; i++) {
					fListener = (IStreamListener) copiedListeners[i];
					SafeRunner.run(this);
				}
				fListener = null;
				fText = null;
			}
		}

		@Override
		public String getContents() {
			return "";
		}
	}
}
