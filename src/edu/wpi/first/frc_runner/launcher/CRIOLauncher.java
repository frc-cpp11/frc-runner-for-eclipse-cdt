package edu.wpi.first.frc_runner.launcher;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;

import edu.wpi.first.frc_runner.Activator;

public class CRIOLauncher implements ILaunchConfigurationDelegate {
	@Override
	public void launch(ILaunchConfiguration configuration, String mode,
			ILaunch launch, final IProgressMonitor monitor)
			throws CoreException {
		String host = CRIOConnectionTab.getIPByTeam(configuration.getAttribute(
				CRIOConnectionTab.TEAM_NUMBER_ID, -1));
		String user = configuration.getAttribute(CRIOConnectionTab.USER_ID,
				"root");
		String pass = configuration.getAttribute(CRIOConnectionTab.USER_ID, "");
		String rawFile = configuration.getAttribute(
				CRIOConnectionTab.FILE_ID, "");
		final File file = new File(rawFile);
		if (rawFile.trim().length() == 0 || file == null || !file.exists()) {
			Activator
					.log(new FileNotFoundException(), "Unable to find binary!");
			monitor.done();
			return;
		}
		monitor.beginTask("Launching...", 8 + (int) (file.length() / 1024));

		monitor.subTask("Connecting to cRIO...");
		FTPClient ftp = new FTPClient();
		try {
			ftp.connect(host);
		} catch (Exception e) {
			Activator.log(e, "Unable to connect to " + host);
			monitor.done();
			return;
		}
		monitor.worked(1);

		monitor.subTask("Logging into cRIO...");
		try {
			ftp.login(user, pass);
		} catch (Exception e) {
			Activator.log(e, "Unable to login to " + user + "@" + host);
			monitor.done();
			return;
		}
		monitor.worked(2);

		monitor.subTask("Sending to robot...");
		try {
			ftp.changeDirectory("/ni-rt/system");
			ftp.upload("FRC_UserProgram.out", new FileInputStream(file), 0,
					0, new FTPDataTransferListener() {
						@Override
						public void started() {
						}

						@Override
						public void transferred(int length) {
							monitor.worked(2 + (length / 1024));
							monitor.subTask("Transfered " + (length / 1024)
									+ "/" + (file.length() / 1024) + "kb");
						}

						@Override
						public void completed() {
						}

						@Override
						public void aborted() {
						}

						@Override
						public void failed() {
						}
					});
		} catch (Exception e) {
			Activator.log(e, "Unable to send file...");
			monitor.done();
			return;
		}
		int offset = (int) (file.length() / 1024);
		monitor.worked(offset + 2);

		monitor.subTask("Rebooting the cRIO...");
		CRIOProcess process = new CRIOProcess(launch, host, host);
		try {
			process.getStreamsProxy().write("reboot");
		} catch (IOException e) {
			Activator.log(e, "Unable to reboot the cRIO...");
			monitor.done();
			return;
		}
		monitor.done();

		launch.addProcess(process);
	}
}
