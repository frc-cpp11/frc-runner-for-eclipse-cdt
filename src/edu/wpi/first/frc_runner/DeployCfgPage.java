package edu.wpi.first.frc_runner;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.PropertyPage;

public class DeployCfgPage extends PropertyPage {
	public static final String TEAM_NUMBER_PROPERTY = "teamNumber";
	public static final String TEAM_NUMBER_NAME = "Team Number";
	public static final String TEAM_NUMBER_ID = "edu.wpi.first.frc_runner.teamNumber";
	
	private Text ownerText;

	public DeployCfgPage() {
		super();
	}

	private void addFirstSection(Composite parent) {
		Composite composite = createDefaultComposite(parent);
		Label pathLabel = new Label(composite, SWT.NONE);
		pathLabel.setText("Team Number");
		Text pathValueText = new Text(composite, SWT.WRAP | SWT.READ_ONLY);
		pathValueText.setText(((IResource) getElement()).getFullPath()
				.toString());
	}

	private void addSeparator(Composite parent) {
		Label separator = new Label(parent, SWT.SEPARATOR | SWT.HORIZONTAL);
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		separator.setLayoutData(gridData);
	}

	private void addSecondSection(Composite parent) {
		Composite composite = createDefaultComposite(parent);
		Label ownerLabel = new Label(composite, SWT.NONE);
		ownerLabel.setText(TEAM_NUMBER_NAME);
		ownerText = new Text(composite, SWT.SINGLE | SWT.BORDER);
		GridData gd = new GridData();
		gd.widthHint = 24;//convertWidthInCharsToPixels(8);
		ownerText.setLayoutData(gd);
		try {
			String owner = ((IResource) getElement())
					.getPersistentProperty(new QualifiedName("",
							TEAM_NUMBER_PROPERTY));
			ownerText.setText(owner);
		} catch (CoreException e) {
		}
	}

	protected Control createContents(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		composite.setLayout(layout);
		GridData data = new GridData(GridData.FILL);
		data.grabExcessHorizontalSpace = true;
		composite.setLayoutData(data);

		addFirstSection(composite);
		addSeparator(composite);
		addSecondSection(composite);
		return composite;
	}

	private Composite createDefaultComposite(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		composite.setLayout(layout);

		GridData data = new GridData();
		data.verticalAlignment = GridData.FILL;
		data.horizontalAlignment = GridData.FILL;
		composite.setLayoutData(data);

		return composite;
	}

	protected void performDefaults() {
		super.performDefaults();
	}

	public boolean performOk() {
		try {
			((IResource) getElement()).setPersistentProperty(new QualifiedName(
					TEAM_NUMBER_ID, TEAM_NUMBER_PROPERTY), ownerText.getText());
		} catch (CoreException e) {
			return false;
		}
		return true;
	}
}